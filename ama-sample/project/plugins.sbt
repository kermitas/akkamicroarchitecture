resolvers += "Sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.7.0-SNAPSHOT")

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.4.1")

addSbtPlugin("com.typesafe.sbt" % "sbt-scalariform" % "1.2.1")

