package as.ama.startup

class InitializationTimeoutException(msg: String) extends RuntimeException(msg)
