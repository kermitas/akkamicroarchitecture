package as.ama.startup

case class InitializationResult(result: Either[Exception, Option[Any]]) extends Serializable